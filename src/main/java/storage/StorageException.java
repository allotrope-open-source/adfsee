package storage;

/**
 * based on https://github.com/b1nnar/examples-spring-boot/blob/master/src/main/java/ro/binnar/projects/alfa/storage/FileSystemStorageService.java
 *
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
public class StorageException extends RuntimeException
{
	private static final long serialVersionUID = 1L;

	public StorageException(String message)
	{
		super(message);
	}

	public StorageException(String message, Throwable cause)
	{
		super(message, cause);
	}
}
