package storage;

/**
 * based on https://github.com/b1nnar/examples-spring-boot/blob/master/src/main/java/ro/binnar/projects/alfa/storage/FileSystemStorageService.java
 *
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
public class StorageFileNotFoundException extends StorageException
{

	private static final long serialVersionUID = 4046310848196554140L;

	public StorageFileNotFoundException(String message)
	{
		super(message);
	}

	public StorageFileNotFoundException(String message, Throwable cause)
	{
		super(message, cause);
	}
}