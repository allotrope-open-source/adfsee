package processor.audit;

import java.util.List;

/**
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
public class DataDescriptionChanges extends AbstractAuditEntity
{
	public String motivatedBy;
	public String subjectOfChange;
	public List<Update> updates;
	public List<DataDescriptionChange> additions;
	public List<DataDescriptionChange> removals;

	public String getMotivatedBy()
	{
		return motivatedBy;
	}

	public void setMotivatedBy(String motivatedBy)
	{
		this.motivatedBy = motivatedBy;
	}

	public String getSubjectOfChange()
	{
		return subjectOfChange;
	}

	public void setSubjectOfChange(String subjectOfChange)
	{
		this.subjectOfChange = subjectOfChange;
	}

	public List<Update> getUpdates()
	{
		return updates;
	}

	public void setUpdates(List<Update> updates)
	{
		this.updates = updates;
	}

	public List<DataDescriptionChange> getAdditions()
	{
		return additions;
	}

	public void setAdditions(List<DataDescriptionChange> additions)
	{
		this.additions = additions;
	}

	public List<DataDescriptionChange> getRemovals()
	{
		return removals;
	}

	public void setRemovals(List<DataDescriptionChange> removals)
	{
		this.removals = removals;
	}
}
