package processor.audit;

import java.util.List;

import org.allotrope.datashapes.prov.model.Agent;

/**
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
public class Change extends AbstractAuditEntity
{
	List<Agent> attributedTo;

	public List<Agent> getAttributedTo()
	{
		return attributedTo;
	}

	public void setAttributedTo(List<Agent> attributedTo)
	{
		this.attributedTo = attributedTo;
	}
}
