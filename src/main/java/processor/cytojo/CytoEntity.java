package processor.cytojo;

/**
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
public class CytoEntity
{
	boolean removed = false;
	boolean selected = false;
	boolean selectable = true;
	boolean locked = false;
	boolean grabbable = true;
	String classes = "l1";
	String clusterId = "1";

	public CytoEntity()
	{
		super();
	}

	public boolean isRemoved()
	{
		return removed;
	}

	public void setRemoved(boolean removed)
	{
		this.removed = removed;
	}

	public boolean isSelected()
	{
		return selected;
	}

	public void setSelected(boolean selected)
	{
		this.selected = selected;
	}

	public boolean isSelectable()
	{
		return selectable;
	}

	public void setSelectable(boolean selectable)
	{
		this.selectable = selectable;
	}

	public boolean isLocked()
	{
		return locked;
	}

	public void setLocked(boolean locked)
	{
		this.locked = locked;
	}

	public boolean isGrabbable()
	{
		return grabbable;
	}

	public void setGrabbable(boolean grabbable)
	{
		this.grabbable = grabbable;
	}

	public String getClasses()
	{
		return classes;
	}

	public void setClasses(String classes)
	{
		this.classes = classes;
	}

	public String getClusterId()
	{
		return clusterId;
	}

	public void setClusterId(String clusterId)
	{
		this.clusterId = clusterId;
	}

}
