package processor.adfinfo;

import java.util.List;

/**
 * @author Helge.Krieg, OSTHUS GmbH
 *
 */
public class HdfStructure
{
	public String name;
	List<HdfEntity> hdfEntities;

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public List<HdfEntity> getHdfEntities()
	{
		return hdfEntities;
	}

	public void setHdfEntities(List<HdfEntity> hdfEntities)
	{
		this.hdfEntities = hdfEntities;
	}
}
