package query;

public class Queries {
	public static final String queryForGraphs = "" //
			+ "SELECT DISTINCT ?g where { \r\n" //
			+ "    GRAPH ?g { \r\n" //
			+ "        ?s ?p ?o . \r\n" //
			+ "    } \r\n" //
			+ " }";

	public static final String queryForNumberOfAllTriples = "" //
			+ "SELECT (COUNT(?s) as ?num) WHERE { \r\n" //
			+ "	{ \r\n" //
			+ "    	GRAPH ?g {\r\n" //
			+ "    		?s ?p ?o . \r\n" //
			+ "  		} \r\n" //
			+ "	} UNION { \r\n" //
			+ "    	?s ?p ?o .\r\n" //
			+ "	}\r\n" //
			+ "}";

	public static final String deleteAllTriples = "" //
			+ "DELETE { \r\n" //
			+ "    ?s ?p ?o . \r\n" //
			+ "} WHERE { \r\n " //
			+ "    ?s ?p ?o . \r\n" //
			+ "}";

	public static final String initMessage = "" //
			+ "SELECT ?s WHERE { \r\n" //
			+ "    BIND(\"Triple store is running and SPARQL endpoint is available at http://localhost:9999/adfsee/sparql\" as ?s) \r\n" //
			+ "}";

}
